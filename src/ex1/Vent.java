package ex1;

abstract public class Vent extends Instrument {

	protected int numAgujeros;
	protected Material material;

	public Vent(String nom, int numAgujeros) {
		super(nom);
		this.numAgujeros = numAgujeros;
	}

	public Vent(String nom, Tama�o tama�o, int numAgujeros) {
		super(nom, tama�o);
		this.numAgujeros = numAgujeros;

	}

	public Vent(String nom, Tama�o tama�o, int numAgujeros, Material material) {
		super(nom, tama�o);
		this.numAgujeros = numAgujeros;
		this.material = material;
	}

}
