package ex1;

public final class Piano extends Corda {

	String marca;

	public Piano(String nom, int numcordes) {
		super(nom, numcordes);
	}

	public Piano(String nom, int numCordes, String marca) {
		super(nom, numCordes);
		this.marca = marca;
	}

	public Piano(String nom, int numcordes, CordaTipo tipos, Tama�o tama�o) {
		super(nom, numcordes, tipos, tama�o);
	}

}
