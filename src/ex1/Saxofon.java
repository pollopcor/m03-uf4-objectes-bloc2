package ex1;

public final class Saxofon extends Vent {

	public Saxofon(String nom, int numAgujeros) {
		super(nom, numAgujeros);
		
	}
	
	public Saxofon(String nom, Tama�o tama�o, int numAgujeros) {
		super(nom, tama�o, numAgujeros);
		
	}
	
	public Saxofon(String nom, Tama�o tama�o, int numAgujeros, Material material) {
		super(nom, tama�o, numAgujeros, material);
		
	}

}
