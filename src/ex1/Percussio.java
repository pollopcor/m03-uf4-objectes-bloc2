package ex1;

abstract public class Percussio extends Instrument {

	public Percussio(String nom) {
		super(nom);
	}
	
	public Percussio(String nom, Tama�o tama�o) {
		super(nom, tama�o);
	}

}
