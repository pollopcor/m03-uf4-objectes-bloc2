package ex1;

public final class Trompeta extends Vent {

	public Trompeta(String nom, int numAgujeros) {
		super(nom, numAgujeros);
	}

	public Trompeta(String nom, Tama�o tama�o, int numAgujeros) {
		super(nom, tama�o, numAgujeros);
	}

	public Trompeta(String nom, Tama�o tama�o, int numAgujeros, Material material) {
		super(nom, tama�o, numAgujeros, material);
	}

}
