package ex1;

abstract public class Corda extends Instrument {

	public int numcordes;
	public CordaTipo tipos;

	public Corda(String nom, int numcordes) {
		super(nom);
		this.numcordes = numcordes;
	}

	public Corda(String nom, int numcordes, CordaTipo tipos, Tama�o tama�o) {
		super(nom, tama�o);
		this.numcordes = numcordes;
		this.tipos = tipos;
	}

}
