package ex1;

abstract public class Instrument {

	protected String nom;
	protected Tama�o tama�o;

	public Instrument(String nom) {
		this.nom = nom;
	}

	public Instrument(String nom, Tama�o tama�o) {
		this(nom);
		this.tama�o = tama�o;
	}

}
